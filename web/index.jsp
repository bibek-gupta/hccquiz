<%-- 
    Document   : index
    Created on : Jun 3, 2018, 1:19:24 PM
    Author     : Bibek Gupta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>Hcc Quiz</title>
    </head>
    <link rel="stylesheet" href="res/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="res/css/style.css"/>
    <link rel="stylesheet" href="res/fonts/font-awesome/css/font-awesome.min.css"/>
    <body>
        <div class="container-fluid body-wrapper">
            <div class="row  justify-content-center ">
                <div class="form-wrapper">
                    <form>
                        <span class="form-title">
                            Login
                        </span>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">Username</label>
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-user"></i></div>
                                    </div>
                                    <input type="text" class="form-control" name="username" id="inlineFormInputGroup" placeholder="Username">
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label class="sr-only" for="inlineFormInputGroup">password</label>
                                <div class="input-group mb-1">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fa fa-lock"></i></div>
                                    </div>
                                    <input type="text" class="form-control" name="password" id="inlineFormInputGroup" placeholder="password">
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <a href="#">
                                Forgot password?
                            </a>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary login-form-btn" style="background-color: #9A98EF;
                                        border-color: #9A98EF">Login</button>
                            </div>
                        </div>
                        <div class="text1 text-center" style="padding-bottom: 0px;">
                            <span>Or</span>
                        </div>
                        <div class="text1 text-center">
                            <span>Sign Up Using</span>
                        </div>
                        <div class="flex-c-m">
                            <a href="#" class="login100-social-item bg1">
                                <i class="fa fa-facebook"></i>
                            </a>

                            <a href="#" class="login100-social-item bg2">
                                <i class="fa fa-twitter"></i>
                            </a>

                            <a href="#" class="login100-social-item bg3">
                                <i class="fa fa-google"></i>
                            </a>
                        </div>
                        <div class="text-center" style="margin-top: 25px;">
                            <a href="Register.jsp">
                                Create New Account
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="res/js/bootstrap.min.js"></script>
</html>
