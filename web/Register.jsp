<%-- 
    Document   : Register
    Created on : Jun 4, 2018, 8:30:42 AM
    Author     : Bibek Gupta
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <title>Create New Account</title>
        <link rel="stylesheet" href="res/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="res/css/style.css"/>
        <link rel="stylesheet" href="res/fonts/font-awesome/css/font-awesome.min.css"/>
    </head>
    <body>
        <div class="container-fluid body-wrapper">
            <div class="row justify-content-center">
                <div class="form-wrapper">
                    <form method="#">
                        <span class="form-title">Create Your Account</span>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label class="sr-only" for="firstname">FirstName</label>
                                <div class="input-group mb-1">
                                    <input type="text" class="form-control" name="first_name"  placeholder="First Name">
                                </div>
                            </div>
                            <div class="col-auto">
                                <label class="sr-only" for="lastname">LastName</label>
                                <div class="input-group mb-1">
                                    <input type="text" class="form-control" name="last_name"  placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-12">
                                <label class="sr-only" for="email">Email</label>
                                <div class="input-group mb-1">
                                    <input type="email" name="email" class="form-control"  placeholder="E-mail"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-12">
                                <label class="sr-only" for="phone">Phone</label>
                                <div class="input-group mb-1">
                                    <input type="tel" name="phone" class="form-control"  placeholder="Phone"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-12">
                                <label class="sr-only" for="pwd">Password</label>
                                <div class="input-group mb-1">
                                    <input type="password" name="password" class="form-control" id="pwd" placeholder="Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-12">
                                <label class="sr-only" for="pwd">ConfirmPassword</label>
                                <div class="input-group mb-1">
                                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-6">
                                <label class="sr-only" for="batch">Batch</label>
                                <div class="input-group mb-1">
                                    <select class="custom-select">
                                        <option value="" selected>Batch</option>
                                        <option value="1">2071</option>
                                        <option value="2">2071</option>
                                        <option value="3">2073</option>
                                        <option value="3">2074</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="sr-only" for="semester">Semester</label>
                                <div class="input-group mb-1">
                                    <select class="custom-select">
                                        <option value="" selected>Semester</option>
                                        <option value="1">1st semester</option>
                                        <option value="2">2nd semester</option>
                                        <option value="3">3rd semester</option>
                                        <option value="3">4th semester</option>
                                        <option value="1">5th semester</option>
                                        <option value="2">6th semester</option>
                                        <option value="3">7th semester</option>
                                        <option value="3">8th semester</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary" style="background-color: #9A98EF;margin-top: 20px;
                                        border-color: #9A98EF">Create Account</button>
                            </div>
                        </div>
                        <div class="text-center" style="margin-top: 25px;">

                            Already Have An Account?

                        </div>
                        <div class="text-center">
                            <a href="index.jsp">
                              Login From Here
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="res/js/bootstrap.min.js"></script>
</html>
